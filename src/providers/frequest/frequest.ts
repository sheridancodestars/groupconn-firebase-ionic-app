import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { User, UserProvider } from '../user/user';
import { AuthProvider } from '../auth/auth';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { _createDefaultCookieXSRFStrategy } from '@angular/http/src/http_module';

export interface Request {
  rid : string;

  from_uid : string;
  to_uid : string;
};

@Injectable()
export class FrequestProvider {

  requests : AngularFirestoreCollection<Request>;

  private _currentUserFriendRequests : Request[];
  private currentUserFriendRequests_sub : Subscription;

  get currentUserFriendRequests() {
    return this._currentUserFriendRequests;
  }

  constructor(private afs : AngularFirestore, private up : UserProvider, private auth : AuthProvider, private user : UserProvider) {
    this.requests = this.afs.collection<Request>("friendrequests");

    auth.isAuthorized$.subscribe((data) => {
      if(data)
      {
        this.currentUserFriendRequests_sub = this.searchIncomingRequests(this.auth.currentFirestoreUser).valueChanges().subscribe((data2) => {
          this._currentUserFriendRequests = data2;
        });
      }
      else
      {
        if(this.currentUserFriendRequests_sub != null)
        {
          this.currentUserFriendRequests_sub.unsubscribe();
        }

        this._currentUserFriendRequests = [];
        this.currentUserFriendRequests_sub = null;
      }
    });
  }

  // Helper functions
  public send(from_user : AngularFirestoreDocument<User>, to_user : AngularFirestoreDocument<User>) {
    let request : Request = {
      rid : "",
      from_uid : from_user.ref.id,
      to_uid : to_user.ref.id
    };

    return this.requests.add(request).then((data) => {
      data.update({
        rid : data.id
      });
    });
  }

  public accept(request : AngularFirestoreDocument<Request>) {
    request.valueChanges().take(1).subscribe((data) => {
      let from_user : AngularFirestoreDocument<User> = this.up.getById(data.from_uid);
      let to_user : AngularFirestoreDocument<User> = this.up.getById(data.to_uid);

      this.up.addFriend(from_user, to_user);
      this.up.addFriend(to_user, from_user);

      request.delete();
    });
  }

  public getById(rid : string) {
    return this.requests.doc(rid) as AngularFirestoreDocument<Request>;
  }

  public searchIncomingRequests(user : AngularFirestoreDocument<User>) {
    return this.afs.collection<Request>("friendrequests", ref => ref.where("to_uid", "==", user.ref.id));
  }

  public searchOutgoingRequests(user : AngularFirestoreDocument<User>) {
    return this.afs.collection<Request>("friendrequests", ref => ref.where("from_uid", "==", user.ref.id));
  }
}
