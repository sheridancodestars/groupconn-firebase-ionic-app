import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';

import { TaskReference, Task } from '../task2/task2';

import {
    AngularFirestore,
    AngularFirestoreCollection,
    AngularFirestoreDocument
} from 'angularfire2/firestore';

import { AuthProvider } from '../auth/auth';
import { UserProvider } from '../user/user';

export interface GroupReference {
  gid : string;
};

export interface Group {
  gid : string;

  name : string;
  avatarURL : string;
};

@Injectable()
export class GroupProvider {

  groups : AngularFirestoreCollection<Group>;

  private _currentUserGroups : GroupReference[];
  private currentUserGroups_sub : Subscription;

  get currentUserGroups() {
    return this._currentUserGroups;
  }

  constructor(private afs : AngularFirestore, private auth : AuthProvider, private user : UserProvider) {
    this.groups = this.afs.collection<Group>("groups");

    auth.isAuthorized$.subscribe((data) => {
      if(data)
      {
        this.currentUserGroups_sub = this.user.getGroups(this.auth.currentFirestoreUser).valueChanges().subscribe((data2) => {
          this._currentUserGroups = data2;
        });
      }
      else
      {
        if(this.currentUserGroups_sub != null)
        {
          this.currentUserGroups_sub.unsubscribe();
        }

        this._currentUserGroups = [];
        this.currentUserGroups_sub = null;
      }
    });
  }

  // Group helper methods
  public push(obj : Group) {
    return this.groups.add(obj).then((data) => {
      data.update({
        gid : data.id
      });

      return data;
    });
  }

  public create(name : string, avatarURL : string) {
    let group : Group = {
      gid : "",
      name : name,
      avatarURL : avatarURL
    };

    return group;
  }

  public getById(gid : string) {
    return this.groups.doc(gid) as AngularFirestoreDocument<Group>;
  }

  public getTasks(group : AngularFirestoreDocument<Group>) {
    return group.collection<TaskReference>("tasks");
  }

  public addTask(group : AngularFirestoreDocument<Group>, task : AngularFirestoreDocument<Task>) {
    let taskref : TaskReference = {
      tid : task.ref.id
    };

    return this.getTasks(group).add(taskref);
  }

  public removeTask(group : AngularFirestoreDocument<Group>, task : AngularFirestoreDocument<Task>) {
    this.getTasks(group).ref.where("tid", "==", task.ref.id).get().then((data) => {
      data.docs.forEach((value, index, array) => {
        value.ref.delete();
      });
    });
  }
}
