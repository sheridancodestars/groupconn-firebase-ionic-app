import { Subscription } from 'rxjs/Subscription';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

import { AuthProvider } from '../auth/auth';
import { UserProvider } from '../user/user';

export interface TaskReference {
  tid : string;
};

export interface Task {
  tid : string;

  taskTitle: string,
  taskReminder: string,
  taskDate: Date,
  taskStartTime: Date,
  taskEndTime: Date,
  taskNote: string,
  taskLocation: string
};

@Injectable()
export class Task2Provider {

  tasks : AngularFirestoreCollection<Task>;

  private _currentUserTasks : TaskReference[];
  private currentUserTasks_sub : Subscription;

  get currentUserTasks() {
    return this._currentUserTasks;
  }

  constructor(private afs : AngularFirestore, private auth : AuthProvider, private user : UserProvider) {
    this.tasks = this.afs.collection<Task>("tasks");

    auth.isAuthorized$.subscribe((data) => {
      if(data)
      {
        this.currentUserTasks_sub = this.user.getTasks(this.auth.currentFirestoreUser).valueChanges().subscribe((data2) => {
          this._currentUserTasks = data2;
        });
      }
      else
      {
        if(this.currentUserTasks_sub != null)
        {
          this.currentUserTasks_sub.unsubscribe();
        }

        this._currentUserTasks = [];
        this.currentUserTasks_sub = null;
      }
    });
  }

  // Task helper functions
  public push(obj : Task) {
    return this.tasks.add(obj).then((data) => {
      data.update({
        tid : data.id
      });

      return data;
    });
  }

  public create(taskTitle : string, taskReminder : string, taskDate : Date, taskStartTime : Date, taskEndTime : Date, taskNote : string, taskLocation : string) {
    let task : Task = {
      tid : "",
      taskTitle: taskTitle,
      taskReminder: taskReminder,
      taskDate: taskDate,
      taskStartTime: taskStartTime,
      taskEndTime: taskEndTime,
      taskNote: taskNote,
      taskLocation: taskLocation
    };

    return task;
  }

  public update(task : AngularFirestoreDocument<Task>, obj : Partial<Task>) {
    return task.update(obj);
  }

  public getById(tid : string) {
    return this.tasks.doc(tid) as AngularFirestoreDocument<Task>;
  }
}
