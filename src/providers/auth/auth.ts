import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';
import 'rxjs/add/operator/map';

import { GooglePlus } from '@ionic-native/google-plus';

import { User, UserProvider } from '../user/user';
import { AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Injectable()
export class AuthProvider {

  private auth$ : Observable<boolean>;
  private authFbUser : firebase.User;
  private authAfsUser : AngularFirestoreDocument<User>;
  private authAfsUser_sub : Subscription;
  private authUser : User;

  get isAuthorized$() {
    return this.auth$;
  }

  get currentFirebaseUser() {
    return this.authFbUser;
  }

  get currentFirestoreUser() {
    return this.authAfsUser;
  }

  get currentUser() {
    return this.authUser;
  }

  constructor(
    private afa : AngularFireAuth,
    private up : UserProvider,
    private gp : GooglePlus
  ) {
    this.auth$ = new Observable(observer => {
      // Push to observer whenever authState changes.
      // If authState != null, push when everything is loaded.
      this.afa.authState.subscribe((data) => {
        if(data != null)
        {
          // Authorized.
          this.authFbUser = data;

          // Load userdata from database, if it exists.
          this.up.searchAuthId(data.uid).then((userdata) => {
            if(!userdata.empty)
            {
              // Userdata found.
              this.authAfsUser = this.up.getById(userdata.docs[0].id);
              this.authUser = userdata.docs[0].data() as User;

              this.authAfsUser_sub = this.authAfsUser.valueChanges().subscribe((authuserdata) => {
                // Update user object when changes are made on the database.
                this.authUser = authuserdata;
              });

              observer.next(true);
            }
            else
            {
              // No userdata.
              this.authUser = this.up.create(
                data.uid,
                data.displayName || data.email,
                data.email,
                data.photoURL || "https://api.adorable.io/avatars/285/" + data.uid + ".png"
              );

              this.up.push(this.authUser).then((docref) => {
                this.authAfsUser = this.up.getById(docref.id);

                this.authAfsUser_sub = this.authAfsUser.valueChanges().subscribe((authuserdata) => {
                  // Update user object when changes are made on the database.
                  this.authUser = authuserdata;
                });

                observer.next(true);
              }).catch(() => {
                observer.next(false);
              });
            }
          });
        }
        else
        {
          // Not authorized.
          // Set authUser variables to null and push to observer.
          if(this.authAfsUser_sub != null)
          {
            this.authAfsUser_sub.unsubscribe();
          }

          this.authAfsUser = null;
          this.authFbUser = null;
          this.authUser = null;

          observer.next(false);
        }
      });
    });
  }

  public registerEmail(email : string, password : string) {
    return this.afa.auth.createUserWithEmailAndPassword(email, password);
  }

  public loginEmail(email : string, password : string) {
    return this.afa.auth.signInWithEmailAndPassword(email, password);
  }

  public loginGoogle() {
    return new Promise((resolve) => {
      this.gp.login().then((obj) => {
        this.afa.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(obj.idToken)).then((data) => {
          resolve(data);
          return data;
        });

        return obj;
      });
    });
  }

  public resetPassword(email : string) {
    return this.afa.auth.sendPasswordResetEmail(email);
  }

  public logout() {
    return this.afa.auth.signOut();
  }

}
