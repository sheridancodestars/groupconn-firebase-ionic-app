import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { GroupReference, Group } from '../group/group';
import { TaskReference, Task } from '../task2/task2';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

export interface UserReference {
  uid : string;
};

export interface User {
  uid : string;

  /* userdata */
  authid : string;
  username : string;
  email : string;
  avatarURL : string;

  /* other userdata */
  newuser : boolean;
};

@Injectable()
export class UserProvider {

  users : AngularFirestoreCollection<User>;
  displayUsers: AngularFirestoreCollection<User>;

  constructor(private afs : AngularFirestore)
  {
    this.users = this.afs.collection<User>("users");
  }

  // User helper functions
  public push(obj : User) {
    return this.users.add(obj).then((data) => {
      data.update({
        uid : data.id
      });

      return data;
    });
  }

  public create(authid : string, username : string, email : string, avatarURL : string) {
    let user : User = {
      uid : "",
      authid : authid,
      username : username,
      email : email,
      avatarURL : avatarURL,
      newuser : true
    };

    return user;
  }

  public update(user : AngularFirestoreDocument<User>, obj : Partial<User>) {
    return user.update(obj);
  }

  public getById(uid : string) {
    return this.users.doc(uid) as AngularFirestoreDocument<User>;
  }

  public getByIds(uids : string[]) {
    let users : AngularFirestoreDocument<User>[] = [];

    uids.forEach((value, index, array) => {
      users.push(this.getById(value));
    });

    return users;
  }

  public getByRefs(userrefs : UserReference[]) {
    let uids : string[] = [];

    userrefs.forEach((value, index, array) => {
      uids.push(value.uid);
    });

    return this.getByIds(uids);
  }

  public searchAuthId(authid : string) {
    return this.users.ref.where("authid", "==", authid).get();
  }

  public getFriends(user : AngularFirestoreDocument<User>) {
    return user.collection<UserReference>("friends");
  }

  public addFriend(user : AngularFirestoreDocument<User>, user_other : AngularFirestoreDocument<User>) {
    let userref : UserReference = {
      uid : user_other.ref.id
    };

    return this.getFriends(user).add(userref);
  }

  public removeFriend(user : AngularFirestoreDocument<User>, user_other : AngularFirestoreDocument<User>) {
    this.getFriends(user).ref.where("uid", "==", user_other.ref.id).get().then((data) => {
      data.docs.forEach((value, index, array) => {
        value.ref.delete();
      });
    });
  }

  public getGroups(user : AngularFirestoreDocument<User>) {
    return user.collection<GroupReference>("groups");
  }

  public addGroup(user : AngularFirestoreDocument<User>, group : AngularFirestoreDocument<Group>) {
    let groupref : GroupReference = {
      gid : group.ref.id
    };

    return this.getGroups(user).add(groupref);
  }

  public removeGroup(user : AngularFirestoreDocument<User>, group : AngularFirestoreDocument<Group>) {
    this.getGroups(user).ref.where("gid", "==", group.ref.id).get().then((data) => {
      data.docs.forEach((value, index, array) => {
        value.ref.delete();
      });
    });
  }

  public getTasks(user : AngularFirestoreDocument<User>) {
    return user.collection<TaskReference>("tasks");
  }

  public addTask(user : AngularFirestoreDocument<User>, task : AngularFirestoreDocument<Task>) {
    let taskref : TaskReference = {
      tid : task.ref.id
    };

    return this.getTasks(user).add(taskref);
  }

  public removeTask(user : AngularFirestoreDocument<User>, task : AngularFirestoreDocument<Task>) {
    this.getTasks(user).ref.where("tid", "==", task.ref.id).get().then((data) => {
      data.docs.forEach((value, index, array) => {
        value.ref.delete();
      });
    });
  }
}
