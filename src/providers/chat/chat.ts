import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import * as firebase from 'firebase';

import { AuthProvider } from '../auth/auth';
import { UserProvider } from '../user/user';
import { Observable } from 'rxjs/Observable';

export interface ChatMessage {
  msg_id : string;
  from_uid : string;
  to_uid : string;
  timestamp : firebase.firestore.FieldValue;
  // types: "m" (message), "t" (task), ...
  type : string;
  data : string;
};

@Injectable()
export class ChatProvider {

  messages : AngularFirestoreCollection<ChatMessage>;

  private currentUserIncomingMessages$ : Observable<ChatMessage>;
  private _currentUserLastMessage : ChatMessage;
  private currentUserIncomingMessages_sub : Subscription;

  // This is an observable which returns the last chat message received.
  get currentUserIncomingMessage$() {
    return this.currentUserIncomingMessage$;
  }

  get currentUserLastMessage() {
    return this._currentUserLastMessage;
  }

  constructor(private afs : AngularFirestore, private auth : AuthProvider, private user : UserProvider) {
    this.messages = afs.collection<ChatMessage>("messages");

    this.currentUserIncomingMessages$ = new Observable((observer) => {
      this.auth.isAuthorized$.subscribe((data) => {
        if(data)
        {
          this.currentUserIncomingMessages_sub = this.get_to(this.auth.currentUser.uid).valueChanges().subscribe((data2) => {
            this._currentUserLastMessage = data2[data2.length - 1];

            observer.next(this.currentUserLastMessage);
          });
        }
        else
        {
          if(this.currentUserIncomingMessages_sub != null)
          {
            this.currentUserIncomingMessages_sub.unsubscribe();
          }

          this._currentUserLastMessage = null;
          this.currentUserIncomingMessages_sub = null;
        }
      });
    });
  }

  public getChatId(from_uid : string, to_uid : string) {
    return from_uid.localeCompare(to_uid) > 0 ? to_uid : from_uid;
  }

  public get_to(to_uid : string) {
    return this.afs.collection<ChatMessage>("messages", (ref) => ref
      .where("to_uid", "==", to_uid)
      .orderBy("timestamp", "asc"));
  }

  public send(from_uid : string, to_uid : string, type : string, data : string) {
    return this.messages.add({
      "msg_id" : "",
      "from_uid" : from_uid,
      "to_uid" : to_uid,
      "timestamp" : null,
      "type" : type,
      "data" : data
    }).then((data) => {
      data.update({
        "msg_id" : data.id,
        "timestamp" : firebase.firestore.FieldValue.serverTimestamp()
      });

      return data;
    });
  }
}
