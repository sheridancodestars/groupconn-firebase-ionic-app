export interface Profile{
    displayName: String;
    email: String;
}