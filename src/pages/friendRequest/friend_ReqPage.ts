import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { User, UserProvider } from '../../providers/user/user';
import { Request, FrequestProvider } from '../../providers/frequest/frequest';
import { AuthProvider } from '../../providers/auth/auth';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';

interface RequestOutput {
  request : AngularFirestoreDocument<Request>;
  user : User;
}

@Component({
  selector: 'page-friends-request',
  templateUrl: 'friend_ReqPage.html'
})
export class FriendsRequestPage {

  userRequests : RequestOutput[];
  requests_sub : Subscription;

  constructor(
    private params: NavParams,
    public navCtrl: NavController,
    private alertCtrl: AlertController, 
    public user: UserProvider,
    private ap : AuthProvider,
    private frq : FrequestProvider,
  ) 
  {
      
  }

  ionViewWillEnter()
  {
    this.requests_sub = this.frq.searchIncomingRequests(this.ap.currentFirestoreUser).snapshotChanges().subscribe((data) => {
      // clear requests
      this.userRequests = [];

      data.forEach((value, index, array) => {
        let request : Request = value.payload.doc.data() as Request;

        this.user.getById(request.from_uid).valueChanges().take(1).subscribe((data) => {
          let output : RequestOutput = {
            request : this.frq.getById(value.payload.doc.id),
            user : data
          };
  
          this.userRequests.push(output);
        });
      });
    });

    console.log("ITS WORKING!!");
  }

  ionViewWillLeave()
  {
    this.requests_sub.unsubscribe();
  }

  addFriend(request : RequestOutput){
    let addAlert = this.alertCtrl.create({
      title: 'Do you want to add the following user?',
      message: request.user.username,
      buttons: [
        {
          text: 'Accept',
          handler: () => 
          {
            this.frq.accept(request.request);
          }
        },
        {
          text: 'Refuse',
          handler: () =>
          {
            request.request.delete();
          }
        }
      ]
    });
    addAlert.present();
  }

}
