import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

import { ChatModalPage } from '../chat-modal/chat-modal';

import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';
import { GroupProvider, Group } from '../../providers/group/group';

import { Subscription } from 'rxjs';

interface usersGroupsDisplay {
  currUsersGroups : Group;
}

@Component({
  selector: 'page-friends-display',
  templateUrl: 'friends_Display.html'
})
export class FriendInfoDisplay {
  fInfo;
  usersGroups : usersGroupsDisplay[];
  groups_sub : Subscription;
  constructor(params: NavParams, public navCtrl: NavController, public modalCtrl: ModalController, public ac: AlertController, public up: UserProvider, public gp: GroupProvider, public ap: AuthProvider) 
  {
      this.fInfo = params.data.fInfo;
  }

  ionViewWillEnter()
  {
    this.up.searchAuthId(this.fInfo.authid).then((data) =>{
      data.forEach((doc) => {
        this.groups_sub = this.up.getGroups(this.up.getById(doc.id)).snapshotChanges().subscribe((data2) => {
          this.usersGroups = [];
          data2.forEach((value, index, array) =>{
            this.gp.getById(value.payload.doc.data().gid).valueChanges().subscribe((data3) =>{
              let output : usersGroupsDisplay = {
                currUsersGroups : data3
              }
              this.usersGroups.push(output);
            });
          });
        });
      });
    });
  }

  ionViewWillLeave()
  {
    this.groups_sub.unsubscribe();
  }

  removeFriend()
  {
    let alert = this.ac.create({
      title: 'Sure you want to remove this friend!',
      buttons: [
        {
          text: 'No',
          handler: () =>{

          }
        },
        {
          text: 'Yes',
          handler: () =>{
            this.up.searchAuthId(this.fInfo.authid).then((data) =>{
              data.forEach((doc) => {
                console.log("Pressed");
                this.up.removeFriend(this.ap.currentFirestoreUser, this.up.getById(doc.id));
              });
              this.navCtrl.pop();
            });
          }
        }
      ]
    });
    alert.present();
  }

  presentChatModal(friendsInfoForChat){
    let chatModal = this.modalCtrl.create(ChatModalPage, {fCInfo : friendsInfoForChat});
    chatModal.present();
  }

}
