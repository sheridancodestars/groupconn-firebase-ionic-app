import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { JoinGroupPage } from '../join-group/join-group';
import { GroupDisplayPage } from '../group-display/group-display';
import { GroupChatModalPage } from '../group-chat-modal/group-chat-modal';

import { Group, GroupProvider } from '../../providers/group/group';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';

interface GroupsOutput {
  usersGroups: Group;
  gid;
}

import { Subscription } from 'rxjs';

/**
 * Generated class for the GroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage {
  
  loadedGroups: GroupsOutput[];
  groupsFiltered: GroupsOutput[];
  groups_sub: Subscription;

  group;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ac: AlertController, public modalCtrl: ModalController, public gp: GroupProvider, public up: UserProvider, public ap: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupsPage');
  }

  ionViewWillEnter(){
    this.groups_sub = this.up.getGroups(this.ap.currentFirestoreUser).snapshotChanges().take(1).subscribe((data) => {
      this.loadedGroups = [];
      this.groupsFiltered = [];
      data.forEach((value, index, array) => {
        this.gp.getById(value.payload.doc.data().gid).valueChanges().take(1).subscribe((data2) =>{
          let output : GroupsOutput = {
            usersGroups : data2,
            gid : value.payload.doc.data().gid
          };

          this.loadedGroups.push(output);
          this.groupsFiltered.push(output);
        });
      })
    }); 
  }

  setGroups()
  {
    this.groupsFiltered = this.loadedGroups;
  }

  ionViewWillLeave()
  {
    this.groups_sub.unsubscribe();
  }

  searchForGroups(ev)
  {
    this.setGroups();
    var inputVal = ev.target.value;

    if(!inputVal){
      return;
    }

    this.groupsFiltered = this.groupsFiltered.filter((data) =>{
      if(data.usersGroups.name && inputVal){
        if(data.usersGroups.name.toLowerCase().indexOf(inputVal.toLowerCase()) > -1){
          return true;
        }
        return false;
      }
    });
  }

  createGroupPrompt(){
    let createGroupPrompt = this.ac.create({
      title: 'Create A Group',
      message: 'Enter a group name',
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Group Name...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

          }
        },
        {
          text: 'Submit',
          handler: data => {
            this.group = this.gp.create(
              data.groupName,
              this.ap.currentUser.avatarURL
            );

            this.gp.push(this.group).then((data) => {
              let alert = this.ac.create({
                title: 'Group has been created',
                buttons: ['Dismiss']
              });
              alert.present();
              
              this.up.addGroup(this.ap.currentFirestoreUser, this.gp.getById(data.id)).then((doc) => {
                let alert = this.ac.create({
                  title: 'User has been added to the group',
                  buttons: ['Dismiss']
                });
                alert.present();
              }).catch((err) => {
                let errAlert = this.ac.create({
                  title: 'There was an error',
                  message: 'Error: ' + err,
                  buttons: ['Dismiss']
                });
                errAlert.present();
              });

            }).catch((err) => {
              let errAlert = this.ac.create({
                title: 'There was an error',
                message: 'Error: ' + err,
                buttons: ['Dismiss']
              });
              errAlert.present();
            });
          }
        }
      ]
    });
    createGroupPrompt.present();
  }

  openGroupChatPanel(groupsInfoForChat, groupsIdInfoForChat){
    let chatModal = this.modalCtrl.create(GroupChatModalPage, {gCInfo: groupsInfoForChat, gCId: groupsIdInfoForChat});
    chatModal.present();
  }

  joinGroupPrompt(){
    this.navCtrl.push(JoinGroupPage);
  }

  getGroupsInfoPage(groupsInfo, groupsGID){
    this.navCtrl.push(GroupDisplayPage, {gInfo : groupsInfo, gId : groupsGID});
  }

}
