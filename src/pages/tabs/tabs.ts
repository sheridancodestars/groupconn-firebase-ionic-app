import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';

import { HomePage } from '../home/home';
import { TasksPage } from '../tasks/tasks';
import { GroupsPage } from '../groups/groups';
import { FriendsPage } from '../friends/friends';
import { NewUserPage } from '../new-user/new-user';
import { ProfilePage} from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage; // Home
  tab2Root = TasksPage; // Tasks
  tab3Root = GroupsPage; // Groups
  tab4Root = FriendsPage; // Friends
  tab5Root = ProfilePage; // Profile

  constructor(
    public navCtrl : NavController,
    public modalCtrl : ModalController,
    public ap : AuthProvider,
    public up : UserProvider
  ) {
    
  }

  showNewUserPage() {
    let modal = this.modalCtrl.create(NewUserPage);
    modal.present();

    //this.navCtrl.push(NewUserPage);
  }

  ionViewDidEnter() {
    if(this.ap.currentUser.newuser)
    {
      this.showNewUserPage();

      this.ap.currentUser.newuser = false;
      this.up.update(this.ap.currentFirestoreUser, this.ap.currentUser);
    }
  }
}
