import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupTaskCreatePage } from './group-task-create';

@NgModule({
  declarations: [
    GroupTaskCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(GroupTaskCreatePage),
  ],
})
export class GroupTaskCreatePageModule {}
