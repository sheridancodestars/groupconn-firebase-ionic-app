import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
declare var google;

import { GroupProvider } from '../../providers/group/group'
import { Task, Task2Provider } from '../../providers/task2/task2'

/**
 * Generated class for the GroupTaskCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group-task-create',
  templateUrl: 'group-task-create.html',
})
export class GroupTaskCreatePage {

  task: Task;
  groupsId;

  tTitle;
  tReminder;
  tDate;
  tStartTime;
  tEndTime;
  tNote;
  tLocation;

  @ViewChild('map') mapElement;
  map: any;
  currLat;
  currLong;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ac: AlertController, public geo: Geolocation, public gp: GroupProvider, public tp: Task2Provider) {
    this.groupsId = navParams.data.groupsId;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupTaskCreatePage');
  }

  openGroupMap(){
    this.geo.getCurrentPosition().then((resp) => {    
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.updateMarker(latLng);          
    }).catch((error) => {
       console.log('Error getting location', error);
    });
  }

  updateMarker(latLng){
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    let marker = new google.maps.Marker({
      position: latLng   
    });
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    marker.setMap(this.map); 
  }

  createTask(){
    this.task = this.tp.create(
      this.tTitle,
      this.tReminder,
      this.tDate,
      this.tStartTime,
      this.tEndTime,
      this.tNote,
      this.tLocation
    );

    this.tp.push(this.task).then((data) => {
      this.gp.addTask(this.gp.getById(this.groupsId), this.tp.getById(data.id)).then((doc) =>{
        let alert = this.ac.create({
          title: 'Groups Task has been created!',
          buttons: ['Dismiss']
        });
        alert.present();
        
        this.tTitle = "";
        this.tReminder = "";
        this.tDate = "";
        this.tStartTime = "";
        this.tEndTime = "";
        this.tNote = "";
        this.tLocation = "";

        this.navCtrl.pop();
      }).catch((err) =>{
        let errAlert = this.ac.create({
          title: 'Error',
          message: 'Error has occured: ' + err,
          buttons: ['Dismiss']
        });
        errAlert.present();
      });
    });
  }

}
