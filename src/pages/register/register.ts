import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from 'ionic-angular';

import 'rxjs/add/operator/take';

import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerForm: FormGroup;
  submitAttempt: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public ap: AuthProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {

  }

  ionViewWillLoad() {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      passwordconfirm: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.submitAttempt = false;

      if (this.registerForm.controls.password.value === this.registerForm.controls.passwordconfirm.value) {
        const alert = this.loadingCtrl.create({
          content: 'Registering...'
        });

        alert.present();

        // submit
        this.ap.registerEmail(
          this.registerForm.controls.email.value,
          this.registerForm.controls.password.value
        )
          .then(() => {
            console.log("success");

            if (this.ap.currentUser != null) {
              // userdata ready
              alert.dismiss();
            }
            else {
              this.ap.isAuthorized$.take(1).subscribe((data) => {
                // firestore finished
                alert.dismiss();

                if (!data) {
                  // couldn't push userdata
                  this.ap.logout();

                  this.alertCtrl.create({
                    title: "Error",
                    subTitle: "Failed to create userdata. Try logging in again later.",
                    buttons: ['OK']
                  }).present();
                }
              });
            }
          })
          .catch((error) => {
            console.log("failed: " + error.message);
            alert.dismiss();

            this.alertCtrl.create({
              title: 'Error',
              subTitle: 'Error when registering: ' + error.message,
              buttons: ['OK']
            }).present();
          })
      }
    }
    else {
      this.submitAttempt = true;
    }
  }

}
