import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { LocationPage } from '../../pages/location/location';
import { UserProvider } from '../../providers/user/user';
import { Task2Provider } from '../../providers/task2/task2';
import { AuthProvider } from '../../providers/auth/auth';
import { TasksPage} from '../../pages/tasks/tasks';
import { Calendar } from '@ionic-native/calendar';

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})

export class AddTaskPage {
  taskPage = TasksPage;

  taskTitle;
  taskReminder;
  taskDate;
  taskStartTime;
  taskEndTime;
  taskNote;
  taskLocation;
  address;
  tasks: any[];

  constructor(private navCtrl: NavController, public modalCtrl: ModalController, public userProvider: UserProvider, public task2Provider: Task2Provider, public authProvider: AuthProvider, private calendar: Calendar) {}

  addTasktoUser(){
    let task = {
      tid : "",
      taskTitle : this.taskTitle,
      taskReminder : this.changeToggle().toString(),
      taskDate : this.taskDate,
      taskStartTime : this.taskStartTime,
      taskEndTime : this.taskEndTime,
      taskNote : this.taskNote,
      taskLocation : this.taskLocation
    }
    this.task2Provider.push(task).then(data => {
      this.userProvider.addTask(this.authProvider.currentFirestoreUser, this.task2Provider.getById(data.id));
    });
  }

  changeToggle(){
    if(this.taskReminder == true){
      return true;
    }else{
      return false;
    }
  }

  getLocation(){
    let modal = this.modalCtrl.create(LocationPage);
    modal.onDidDismiss((location) => {
      this.taskLocation = location;
      this.address = location;
    });
    modal.present();
  }

  addTaskToCalendar(){
    var title = this.taskTitle;
    var location = this.taskLocation;
    var notes = this.taskNote;
    var startDate = this.taskDate; // still need motification
    var endDate = this.taskDate; // still need modification
    var options = this.calendar.getCalendarOptions();
    options.recurrence = "weekly";
    options.firstReminderMinutes = 5;
    options.secondReminderMinutes = 0;
    this.calendar.createEventWithOptions(title,location,notes,startDate,endDate,options).then(
      (msg) => { console.log(msg); },
      (err) => { console.log(err); }
    )

  }

  addEvent1(){
    var title = this.taskTitle;
    var location = this.taskLocation;
    var notes = this.taskNote;
    //new Date(year, month, day, hours, minutes, seconds, milliseconds)
    // month = 0 -> jan
    // hour = 15 -> 3pm
    var startDate = new Date(2017, 11, 10, 15,0,0,0);
    var endDate = new Date(2017, 11, 10, 16,0,0,0);
    var options = this.calendar.getCalendarOptions();
    options.recurrence = "weekly";
    options.firstReminderMinutes = 5; // 15 minute before
    options.secondReminderMinutes = 0; // 5 minute before
    Date.now();
    this.calendar.createEventWithOptions(title,location,notes,startDate,endDate,options).then(
      (msg) => { console.log(msg); },
      (err) => { console.log(err); }
    )
  }
}
