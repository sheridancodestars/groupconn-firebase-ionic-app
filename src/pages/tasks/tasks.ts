import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddTaskPage } from '../add-task/add-task'
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';
import { Task, Task2Provider } from '../../providers/task2/task2';
import { TaskDisplayPage } from '../task-display/task-display';

interface TaskOutput {
  usersTasks: Task;
  taskID;
}

@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html'
})

export class TasksPage {
  addTask = AddTaskPage;
  taskDisplay = TaskDisplayPage;
  tasks: TaskOutput[];

  constructor(public navCtrl: NavController, private userProvider: UserProvider, private authProvider: AuthProvider, private task2provider: Task2Provider){}

  ionViewDidEnter() {
    this.userProvider.getTasks(this.authProvider.currentFirestoreUser).snapshotChanges().subscribe(data => {
      this.tasks = [];
      data.forEach((value, index, array /* == data */) => {
        array[index]; // == value
        this.task2provider.getById(value.payload.doc.data().tid).valueChanges().subscribe((data2) => {
          let output : TaskOutput = {
            usersTasks : data2,
            taskID : value.payload.doc.data().tid
          }
          this.tasks.push(output);
        })
      })
    });
  }

  deleteTask(taskId){
    this.userProvider.removeTask(this.authProvider.currentFirestoreUser, this.task2provider.getById(taskId));
  }

  goToTaskDisplay(idTask, taskDetail){
    this.navCtrl.push(TaskDisplayPage, {taskId: idTask, taskInfo: taskDetail});
  }

}


