import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupChatModalPage } from './group-chat-modal';

@NgModule({
  declarations: [
    GroupChatModalPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupChatModalPage),
  ],
})
export class GroupChatModalPageModule {}
