import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { User, UserProvider } from '../../providers/user/user';
import { ChatMessage, ChatProvider } from '../../providers/chat/chat'

/**
 * Generated class for the GroupChatModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
interface ChatOutput {
  usersMessages: ChatMessage;
  usersName;
}

@IonicPage()
@Component({
  selector: 'page-group-chat-modal',
  templateUrl: 'group-chat-modal.html',
})
export class GroupChatModalPage {

  groupsChatInfo;
  groupsChatIdInfo;
  currUser: User;
  currAuthid;
  msgToSend;
  messages;

  constructor(public navCtrl: NavController, public navParams: NavParams, public vc: ViewController, public ap: AuthProvider, public up: UserProvider, public cp: ChatProvider) 
  {
    this.groupsChatInfo = this.navParams.get('gCInfo');
    this.groupsChatIdInfo = this.navParams.get('gCId');
    console.log(this.groupsChatInfo);
  }

  ionViewWillEnter(){
    this.currUser = this.ap.currentUser
    this.currAuthid = this.currUser.authid
    console.log(this.currUser);

    this.cp.get_to(this.groupsChatIdInfo).valueChanges().subscribe((data) =>{
      this.messages = [];
      data.forEach((value) =>{
        this.messages.push(value);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatModalPage');
  }

  sendMessage(){
    if(this.msgToSend != null && this.msgToSend != ""){
      
      this.cp.send(this.ap.currentUser.authid, this.groupsChatIdInfo, "M", this.msgToSend)

      this.msgToSend = "";
    }
  }

  closeGroupChatModal(){
    this.vc.dismiss();
  }

}
