import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupDisplayPage } from './group-display';

@NgModule({
  declarations: [
    GroupDisplayPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupDisplayPage),
  ],
})
export class GroupDisplayPageModule {}
