import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

import { Group, GroupProvider } from '../../providers/group/group';
import { Task, Task2Provider } from '../../providers/task2/task2';
import { User, UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';

import { FriendsAddPage } from '../addFriend/friend_AddPage';
import { GroupChatModalPage } from '../group-chat-modal/group-chat-modal';
import { GroupTaskCreatePage } from '../group-task-create/group-task-create';
import { GroupTaskViewUpdatePage } from '../group-task-view-update/group-task-view-update';

import { Subscription } from 'rxjs';
import { Button } from 'ionic-angular/components/button/button';
import { messaging } from 'firebase';

interface FriendsOutput {
  usersFriends: User;
}

interface TasksOutput {
  groupsTasks: Task;
  groupTid;
}

/**
 * Generated class for the GroupDisplayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group-display',
  templateUrl: 'group-display.html',
})
export class GroupDisplayPage {

  gInfo;
  gId;

  Friends: FriendsOutput[];
  friends_sub: Subscription;

  Tasks: TasksOutput[]; 
  tasks_sub: Subscription;

  constructor(params: NavParams, public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public gp: GroupProvider, public tp: Task2Provider, public up: UserProvider, public ap: AuthProvider, public ac: AlertController) {
    this.gInfo = params.data.gInfo;
    this.gId = params.data.gId;
    console.log(this.gId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupDisplayPage');
  }

  ionViewWillEnter(){
    this.friends_sub = this.up.getFriends(this.ap.currentFirestoreUser).snapshotChanges().subscribe((data) => {
      this.Friends = [];
      data.forEach((value, index, array) => {
        this.up.getById(value.payload.doc.data().uid).valueChanges().subscribe((data2) =>{
          let output : FriendsOutput = {
            usersFriends : data2
          }
          this.Friends.push(output);
        });
      })
    }); 

    this.tasks_sub = this.gp.getTasks(this.gp.getById(this.gId)).snapshotChanges().subscribe((data3) =>{
      this.Tasks = [];
      data3.forEach((value, index, array) => {
        this.tp.getById(value.payload.doc.data().tid).valueChanges().subscribe((data4) =>{
          let output2 : TasksOutput = {
            groupsTasks : data4,
            groupTid : value.payload.doc.data().tid
          }
          this.Tasks.push(output2);
        })
      });
    });
  }

  ionViewWillLeave()
  {
    this.friends_sub.unsubscribe();
    this.tasks_sub.unsubscribe();
  }

  addToGroup(friend){
    this.up.searchAuthId(friend.authid).then((data) =>{
      this.up.addGroup(this.up.getById(data.docs[0].id), this.gp.getById(this.gId)).then((data) =>{
        let alert = this.ac.create({
          title: 'Friend has been added',
          message: 'Friend ' + friend.username + ' has been added to the group',
          buttons: ['Dismiss']
        });
        alert.present();
      }).catch((err) =>{
        let erralert = this.ac.create({
          title: 'Error',
          message: 'Error has occured: ' + err,
          buttons: ['Dismiss']
        });
        erralert.present();
      });
    })
  }

  presentGroupChatModal(){
    let groupChatModal = this.modalCtrl.create(GroupChatModalPage, {gCInfo : this.gInfo, gCId : this.gId});
    groupChatModal.present();
  }

  createGroupTasks(){
    this.navCtrl.push(GroupTaskCreatePage, {groupsId : this.gId});
  }

  viewUpdateGroupTasks(gTid, gTask){
    this.navCtrl.push(GroupTaskViewUpdatePage, {grTid : gTid, grTsk : gTask});
  }

  deleteGroupTasks(gTidD){
    let deleteTaskAlert = this.ac.create({
      title: 'Delete current selected task!',
      buttons: [
        {
          text: 'No',
          handler: () =>{
            
          }
        },
        {
          text: 'Yes',
          handler: () =>{
            this.gp.removeTask(this.gp.getById(this.gId), this.tp.getById(gTidD));
          }
        }
      ]
    });
    deleteTaskAlert.present();
  }

  leaveGroup(){
    let leaveAlert = this.ac.create({
      title: 'Sure you want to leave!',
      buttons: [
        {
          text: 'No',
          handler: () =>{

          }
        },
        {
          text: 'Yes',
          handler: () =>{
            this.up.removeGroup(this.ap.currentFirestoreUser, this.gp.getById(this.gId));
            this.navCtrl.pop();
          }
        }
      ]
    });
    leaveAlert.present();
  }

}
