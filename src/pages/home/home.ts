import { Component } from '@angular/core';
import { NavController, ActionSheetController, ModalController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';

import { NewUserPage } from '../new-user/new-user';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public modalCtrl : ModalController,
    public actionSheetCtrl: ActionSheetController,
    public ap : AuthProvider
  ) {
    
  }

  presentActionSheet() {
    this.actionSheetCtrl.create({
      title: 'User Options',
      buttons: [
        {
          text: 'Logout',
          role: 'logout',
          handler: () => this.ap.logout()
        },
        {
          text: 'Update Profile',
          role: 'edit-profile',
          handler: () => {
            let modal = this.modalCtrl.create(NewUserPage);
            modal.present();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("cancel");
          }
        }
      ]
    }).present();
  }
}
