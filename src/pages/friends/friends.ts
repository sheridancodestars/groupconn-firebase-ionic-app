import { Component } from '@angular/core';
import { NavController, AlertController, ModalController } from 'ionic-angular';
import { FriendInfoDisplay } from '../displayFriends/friends_Display';
import { FriendsRequestPage } from '../friendRequest/friend_ReqPage';
import { FriendsAddPage } from '../addFriend/friend_AddPage';
import { ChatModalPage } from '../chat-modal/chat-modal';

import { User, UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';
import { DocumentChangeAction } from 'angularfire2/firestore/interfaces';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';

interface FriendsOutput {
  usersFriends: User;
}

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html'
})
export class FriendsPage {

  LoadedFriends: FriendsOutput[];
  FriendsFiltered: FriendsOutput[];
  friends_sub: Subscription;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public modalCtrl: ModalController, private afs : AngularFirestore, private usr : UserProvider, private ap : AuthProvider) 
  {
      
  }

  ionViewWillEnter()
  {
    this.friends_sub = this.usr.getFriends(this.ap.currentFirestoreUser).snapshotChanges().take(1).subscribe((data) => {
      this.LoadedFriends = [];
      this.FriendsFiltered = [];
      data.forEach((value, index, array) => {
        this.usr.getById(value.payload.doc.data().uid).valueChanges().take(1).subscribe((data2) =>{
          let output : FriendsOutput = {
            usersFriends : data2
          }
          this.LoadedFriends.push(output);
          this.FriendsFiltered.push(output);
        });
      })
    }); 
  }

  setFriends()
  {
    this.FriendsFiltered = this.LoadedFriends;
  }

  ionViewWillLeave()
  {
    this.friends_sub.unsubscribe();
  }

  searchName(ev)
  {
    this.setFriends();
    var inputVal = ev.target.value;

    if(!inputVal){
      return;
    }

    this.FriendsFiltered = this.FriendsFiltered.filter((data) =>{
      if(data.usersFriends.username && inputVal){
        if(data.usersFriends.username.toLowerCase().indexOf(inputVal.toLowerCase()) > -1){
          return true;
        }
        return false;
      }
    });
  }

  addFriendPrompt(){
    this.navCtrl.push(FriendsAddPage);
  }

  showFriendRequests(){
    this.navCtrl.push(FriendsRequestPage);
  }

  getFriendsInfoPage(friendsInfo){
    this.navCtrl.push(FriendInfoDisplay, {fInfo : friendsInfo})
  }

  openChatPanel(friendsInfoForChat){
    let chatModal = this.modalCtrl.create(ChatModalPage, {fCInfo: friendsInfoForChat});
    chatModal.present();
  }

}
