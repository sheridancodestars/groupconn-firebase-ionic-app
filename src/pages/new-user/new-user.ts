import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  AlertController,
  ToastController,
  LoadingController
} from 'ionic-angular';

import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { AuthProvider } from '../../providers/auth/auth';
import { User, UserProvider } from '../../providers/user/user';

import { Platform } from 'ionic-angular';

import { FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-new-user',
  templateUrl: 'new-user.html',
})
export class NewUserPage {

  profileForm : FormGroup;
  captureDataUrl: string;

  private avatars : firebase.storage.Reference;

  constructor(
    public navCtrl : NavController,
    public viewCtrl : ViewController,
    public navParams : NavParams,
    public formBuilder : FormBuilder,
    public alertCtrl : AlertController,
    public toastCtrl : ToastController,
    public loadingCtrl : LoadingController,
    public ap : AuthProvider,
    public up : UserProvider,
    public camera : Camera,
    public file : File,
    public app : FirebaseApp,
    public plt : Platform
  ) {
    this.avatars = this.app.storage().ref().child("avatars");
  }

  ionViewWillLoad() {
    this.profileForm = this.formBuilder.group({
      avatarURL : ['', Validators.required],
      username : ['', Validators.compose([Validators.minLength(3), Validators.required])]
    });
  }

  ionViewDidEnter() {
    if(this.ap.currentUser == null)
    {
      this.navCtrl.popToRoot();
    }
  }

  updateProfile() {
    if(this.profileForm.valid)
    {
      let avatarURL : string = this.profileForm.controls.avatarURL.value;

      console.log(avatarURL);

      // get user object
      if(avatarURL.startsWith("file:"))
      {
        // Upload this image to firebase.
        const alert = this.loadingCtrl.create({
          content : "Uploading image..."
        });

        alert.present();

        fetch(this.profileForm.controls.avatarURL.value).then((data) => {
          data.blob().then((blob) => {
            this.avatars.child(this.ap.currentUser.authid + ".png").put(blob, {
              contentType : "image/png"
            }).then((data) => {
              alert.dismiss();

              let update_user : Partial<User> = {
                "avatarURL" : data.downloadURL,
                "username" : this.profileForm.controls.username.value
              };

              // save values
              this.up.update(this.ap.currentFirestoreUser, update_user).then((data) => {
                this.toastCtrl.create({
                  message : "Updated profile!",
                  duration: 3000,
                  position: "top"
                }).present();

                this.navCtrl.pop();
              }).catch(() => {
                this.alertCtrl.create({
                  title : "Error",
                  subTitle : "Failed to update profile. Try again later.",
                  buttons : [ 'OK' ]
                }).present();
              });
            }).catch((err) => {
              alert.dismiss();

              this.alertCtrl.create({
                title : "Error",
                subTitle : "Failed to upload image. Try again later.",
                buttons : [ 'OK' ]
              }).present();
            });
          });
        }).catch((err) => {
          alert.dismiss();

          this.alertCtrl.create({
            title : "Error",
            subTitle : "Failed to load image. Try again later.",
            buttons : [ 'OK' ]
          }).present();
        });
      }
      else
      {

        let update_user : Partial<User> = {
          "avatarURL" : this.profileForm.controls.avatarURL.value,
          "username" : this.profileForm.controls.username.value
        };

        // save values
        this.up.update(this.ap.currentFirestoreUser, update_user).then((data) => {
          this.toastCtrl.create({
            message : "Updated profile!",
            duration: 3000,
            position: "top"
          }).present();

          this.navCtrl.pop();
        }).catch(() => {
          this.alertCtrl.create({
            title : "Error",
            subTitle : "Failed to update profile. Try again later.",
            buttons : [ 'OK' ]
          }).present();
        });
      }
    }
    else
    {
      this.alertCtrl.create({
        title : "Error",
        subTitle : "Missing required fields.",
        buttons : [ 'OK' ]
      }).present();
    }
  }

  selectPicture() {
    let destType = this.camera.DestinationType.DATA_URL;

    if(this.plt.is('ios'))
    {
      destType = this.camera.DestinationType.NATIVE_URI;
    }

    const options : CameraOptions = {
      quality : 100,
      destinationType : destType,
      sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType : this.camera.EncodingType.PNG,
      mediaType : this.camera.MediaType.PICTURE,
      allowEdit : true,
      targetHeight : 285,
      targetWidth : 285
    };

    this.camera.getPicture(options).then((data) => {
      if(destType == this.camera.DestinationType.DATA_URL)
      {
        this.profileForm.controls.avatarURL.setValue('data:image/jpeg;base64,' + data);
      }
      else
      {
        this.file.resolveLocalFilesystemUrl(data).then((entry) => {
          this.profileForm.controls.avatarURL.setValue(entry.nativeURL);
        });
      }
    }, (err) => {
      // Handle error
      this.toastCtrl.create({
        message : "Didn't select profile!",
        duration: 3000,
        position: "top"
      }).present();
    });
  }

  takePicture() {
    let destType = this.camera.DestinationType.DATA_URL;

    if(this.plt.is('ios'))
    {
      destType = this.camera.DestinationType.NATIVE_URI;
    }

    const options : CameraOptions = {
      quality : 100,
      destinationType : destType,
      sourceType : this.camera.PictureSourceType.CAMERA,
      encodingType : this.camera.EncodingType.PNG,
      mediaType : this.camera.MediaType.PICTURE,
      allowEdit : true,
      targetHeight : 285,
      targetWidth : 285
    };

    this.camera.getPicture(options).then((data) => {
      if(destType == this.camera.DestinationType.DATA_URL)
      {
        this.profileForm.controls.avatarURL.setValue('data:image/jpeg;base64,' + data);
      }
      else
      {
        this.file.resolveLocalFilesystemUrl(data).then((entry) => {
          this.profileForm.controls.avatarURL.setValue(entry.nativeURL);
        });
      }
    }, (err) => {
      // Handle error
      this.toastCtrl.create({
        message : "Didn't select profile!",
        duration: 3000,
        position: "top"
      }).present();
    });
  }

  setDefaultAvatar() {
    this.profileForm.controls.avatarURL.setValue("https://api.adorable.io/avatars/285/" + this.ap.currentUser.authid + ".png");
  }

}
