import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { GroupProvider } from '../../providers/group/group';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';

import * as _ from 'lodash';

/**
 * Generated class for the JoinGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-join-group',
  templateUrl: 'join-group.html',
})
export class JoinGroupPage {

  loadedJoinGroups;
  filteredJoinGroups;

  loadedGroups;

  constructor(public navCtrl: NavController, public navParams: NavParams, public gp: GroupProvider, public up: UserProvider, public ap: AuthProvider, public ac: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JoinGroupPage');
  }

  ionViewWillEnter()
  {
    this.gp.groups.ref.orderBy('name').get().then((data1) =>{
      this.loadedJoinGroups = [];
      this.filteredJoinGroups = [];
      data1.docs.forEach((value1, index1, array1) =>{
        this.loadedJoinGroups.push(value1);
        this.filteredJoinGroups.push(value1);
        console.log("All Groups " + value1.data());
      });
    });

    this.up.getGroups(this.ap.currentFirestoreUser).ref.get().then((data2) =>{
      this.loadedGroups = [];
      data2.docs.forEach((value2, index2, array2) =>{
        this.gp.getById(value2.data().gid).ref.get().then((data3) =>{
          this.loadedGroups.push(data3);
          console.log("Current Users Groups " + data3.data().name);
        });
      });
    });
  }

  doRefresh(refresher) 
  {
    console.log('Begin async operation', refresher);

    this.gp.groups.ref.orderBy('name').get().then((data1) =>{
      this.loadedJoinGroups = [];
      this.filteredJoinGroups = [];
      data1.docs.forEach((value1, index1, array1) =>{
        this.loadedJoinGroups.push(value1);
        this.filteredJoinGroups.push(value1);
      });
    });

    this.up.getGroups(this.ap.currentFirestoreUser).ref.get().then((data2) =>{
      this.loadedGroups = [];
      data2.docs.forEach((value2, index2, array2) =>{
        this.gp.getById(value2.data().gid).ref.get().then((data3) =>{
          this.loadedGroups.push(data3);
        });
      });
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  setJoinGroups()
  {
    this.filteredJoinGroups = this.loadedJoinGroups;
  }

  searchForJoinGroups(ev)
  { 
    this.setJoinGroups();
    let inputVal = ev.target.value;

    if(!inputVal){
      return;
    }

    this.filteredJoinGroups = this.filteredJoinGroups.filter((data) =>{
      if(data.data().name && inputVal){
        if(data.data().name.toLowerCase().indexOf(inputVal.toLowerCase()) > -1){
          return true;
        }
        return false;
      }
    });
  }

  joinTheGroup(groupInfo){
    let alert = this.ac.create({
      title: 'Join group ' + groupInfo.data().name,
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

          }
        },
        {
          text: 'Confirm',
          handler: data =>{
            this.up.addGroup(this.ap.currentFirestoreUser, this.gp.getById(groupInfo.id)).then((da) =>{
              let addalert = this.ac.create({
                title: 'Joined group ' + groupInfo.data().name,
                buttons: ['Dismiss'] 
              });
              addalert.present();
            }).catch((err) =>{
              let erralert = this.ac.create({
                title: 'Error',
                message: 'Error has occured: ' + err,
                buttons: ['Dismiss']
              });
              erralert.present();
            });
          }
        }
      ]
    });
    alert.present();
  }

}
