import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
declare var google;

import { Task, Task2Provider } from '../../providers/task2/task2';

/**
 * Generated class for the GroupTaskViewUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group-task-view-update',
  templateUrl: 'group-task-view-update.html',
})
export class GroupTaskViewUpdatePage {

  task: Task;
  grTid;
  grTsk;

  tTitle;
  tReminder;
  tDate;
  tStartTime;
  tEndTime;
  tNote;
  tLocation;

  @ViewChild('map') mapElement;
  map: any;
  currLat;
  currLong;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ac: AlertController, public geo: Geolocation, public tp: Task2Provider) {
    this.grTid = navParams.data.grTid;
    this.grTsk = navParams.data.grTsk;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupTaskViewUpdatePage');
  }

  ionViewWillEnter(){
    console.log(this.grTid);
    console.log(this.grTsk);
  }

  openGroupMap(){
    this.geo.getCurrentPosition().then((resp) => {    
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.updateMarker(latLng);          
    }).catch((error) => {
       console.log('Error getting location', error);
    });
  }

  updateMarker(latLng){
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    let marker = new google.maps.Marker({
      position: latLng   
    });
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    marker.setMap(this.map); 
  }

  updateTask(){
    this.task = this.tp.create(
      this.tTitle,
      this.tReminder,
      this.tDate,
      this.tStartTime,
      this.tEndTime,
      this.tNote,
      this.tLocation
    );

    this.tp.update(this.tp.getById(this.grTid), this.task).then((data) =>{
      let alert = this.ac.create({
        title: 'Groups Task has been updated!',
        buttons: ['Dismiss']
      });
      alert.present();

      this.tTitle = "";
      this.tReminder = "";
      this.tDate = "";
      this.tStartTime = "";
      this.tEndTime = "";
      this.tNote = "";
      this.tLocation = "";

      this.navCtrl.pop();
    }).catch((err) =>{
      console.log(err);
    });
  }

}
