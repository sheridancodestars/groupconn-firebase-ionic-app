import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupTaskViewUpdatePage } from './group-task-view-update';

@NgModule({
  declarations: [
    GroupTaskViewUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(GroupTaskViewUpdatePage),
  ],
})
export class GroupTaskViewUpdatePageModule {}
