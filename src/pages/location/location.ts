import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ViewController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  autocompleteItems;
  autocomplete;
  service = new google.maps.places.AutocompleteService();
  autocompleteService: any;
  placesService: any;
  query: string = '';
  places: any = [];
  location: any; 

  @ViewChild('map') mapElement: ElementRef;;
  map: any;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation, private zone: NgZone) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    }; 
  }

  ionViewDidLoad(): void {
    this.autocompleteService = new google.maps.places.AutocompleteService();
    this.placesService = new google.maps.places.PlacesService(this.map);
    this.init();
  }
  init(){
    this.geolocation.getCurrentPosition().then((resp) => {    
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.updateMarker(latLng);          
    }).catch((error) => {
       console.log('Error getting location', error);
    });   
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {  
   this.viewCtrl.dismiss(item);
  }

  selectPlace(place){
    this.places = [];
    let location = {
        lat: null,
        lng: null,
        name: place.name
    };

    this.placesService.getDetails({placeId: place.place_id}, (details) => {
      this.zone.run(() => {
          location.name = details.name;
          location.lat = details.geometry.location.lat();
          location.lng = details.geometry.location.lng();
          this.map.map.setCenter({lat: location.lat, lng: location.lng});
          this.location = location;
      });
    });  
  }
    
  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: 
      {country: 'CA'} }, function (predictions, status) {
      me.autocompleteItems = []; 
      me.zone.run(function () {
        predictions.forEach(function (prediction) {
          me.autocompleteItems.push(prediction.description);
        });        
      });
    });
  }

  updateMarker(latLng){
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    let marker = new google.maps.Marker({
      position: latLng   
    });
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    marker.setMap(this.map); 
  }

}

