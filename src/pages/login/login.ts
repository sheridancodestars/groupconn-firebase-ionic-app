import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ToastController,
  LoadingController
} from 'ionic-angular';

import 'rxjs/add/operator/take';

import { AuthProvider } from '../../providers/auth/auth';

import { RegisterPage } from '../register/register';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  registerPage = RegisterPage;

  loginForm: FormGroup;
  submitAttempt: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public ap: AuthProvider
  ) {

  }

  ionViewWillLoad() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  forgotPassword() {
    this.alertCtrl.create({
      title: 'Forgot Password',
      message: 'Enter your email to request a password reset.',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {

          }
        },
        {
          text: 'Reset',
          handler: data => {
            this.ap.resetPassword(data.email).then((value) => {
              this.toastCtrl.create({
                message: 'Password reset email requested.',
                duration: 3000,
                position: 'top'
              }).present();
            }).catch((error) => {
              this.toastCtrl.create({
                message: 'Failed to request password reset email.',
                duration: 3000,
                position: 'top'
              }).present();
            });
          }
        }
      ]
    }).present();
  }

  googleLogin() {
    this.ap.loginGoogle().then((data) => {
      if (this.ap.currentUser == null) {
        this.ap.isAuthorized$.take(1).subscribe((data) => {
          // firestore finished
          if (!data) {
            // couldn't push userdata
            this.ap.logout();

            this.alertCtrl.create({
              title: "Error",
              subTitle: "Failed to create userdata. Try logging in again later.",
              buttons: ['OK']
            }).present();
          }
        });
      }
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.submitAttempt = false;

      const alert = this.loadingCtrl.create({
        content: 'Logging in...'
      });

      alert.present();

      console.log("authenticate");

      this.ap.loginEmail(
        this.loginForm.controls.email.value,
        this.loginForm.controls.password.value
      ).then((data) => {
        if (this.ap.currentUser != null) {
          // userdata ready
          alert.dismiss();
        }
        else {
          this.ap.isAuthorized$.take(1).subscribe((data) => {
            // firestore finished
            alert.dismiss();

            if (!data) {
              // couldn't push userdata
              this.ap.logout();

              this.alertCtrl.create({
                title: "Error",
                subTitle: "Failed to create userdata. Try logging in again later.",
                buttons: ['OK']
              }).present();
            }
          });
        }
      }).catch((error) => {
        console.log(error);

        alert.dismiss();

        this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Incorrect email and/or password.',
          buttons: ['OK']
        }).present();
      });
    }
    else {
      this.submitAttempt = true;
    }
  }

}
