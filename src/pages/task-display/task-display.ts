import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';
import { Task, Task2Provider } from '../../providers/task2/task2';
import { LocationPage } from '../../pages/location/location';

@IonicPage()
@Component({
  selector: 'page-task-display',
  templateUrl: 'task-display.html',
})

export class TaskDisplayPage {
  taskId;
  taskInfo;
  uTask: Partial<Task>;
  taskTitle;
  taskReminder;
  taskDate;
  taskStartTime;
  taskEndTime;
  taskNote;
  taskLocation;
  address;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public navParams: NavParams, public userProvider: UserProvider,
    public task2Provider: Task2Provider, public authProvider: AuthProvider)
  {
    this.taskId = navParams.data.taskId;
    this.taskInfo = navParams.data.taskInfo;
  }

  ionViewWillEnter(){
    // console.log(this.taskInfo);
  }

  changeToggle(){
    if(this.taskReminder == true){
      return true;
    }else{
      return false;
    }
  }

  updateTask(){
    this.uTask = {
      taskTitle : this.taskTitle,
      taskReminder : this.changeToggle().toString(),
      taskDate : this.taskDate,
      taskStartTime : this.taskStartTime,
      taskEndTime : this.taskEndTime,
      taskNote : this.taskNote,
      taskLocation : this.taskLocation
    }
   var t1 = this.task2Provider.update(this.task2Provider.getById(this.taskId), this.uTask);
   console.log(t1);
  }

  getLocation(){
    let modal = this.modalCtrl.create(LocationPage);
    modal.onDidDismiss((location) => {
      this.taskLocation = location;
      this.address = location;
    });
    modal.present();
  }
}
