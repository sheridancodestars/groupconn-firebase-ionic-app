import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskDisplayPage } from './task-display';

@NgModule({
  declarations: [
    TaskDisplayPage,
  ],
  imports: [
    IonicPageModule.forChild(TaskDisplayPage),
  ],
})
export class TaskDisplayPageModule {}
