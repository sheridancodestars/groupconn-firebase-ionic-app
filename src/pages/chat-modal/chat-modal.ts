import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { User, UserProvider } from '../../providers/user/user';
import { Task, Task2Provider } from '../../providers/task2/task2';
import { ChatProvider } from '../../providers/chat/chat'

/**
 * Generated class for the ChatModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface taskOutput {
  usersTasks: Task,
  taskTid;
}

interface message {
  messages;
  tasks;
}

@IonicPage()
@Component({
  selector: 'page-chat-modal',
  templateUrl: 'chat-modal.html',
})
export class ChatModalPage {

  friendsChatInfo;
  currUser: User;
  currAuthid;
  msgToSend;
  messages;
  tasks;
  usersTasks;
  typeMessage = "M";
  typeTask = "T";

  constructor(public navCtrl: NavController, public navParams: NavParams, public vc: ViewController, public ap: AuthProvider, public up: UserProvider, public tp: Task2Provider, public cp: ChatProvider) 
  {
    this.friendsChatInfo = this.navParams.get('fCInfo');
  }

  ionViewWillEnter(){
    this.currUser = this.ap.currentUser
    this.currAuthid = this.currUser.authid

    //messages
    this.cp.get_to(this.cp.getChatId(this.ap.currentUser.authid, this.friendsChatInfo.authid)).valueChanges().subscribe((data) =>{
      this.messages = [];
      this.tasks = [];
      data.forEach((value) =>{
        switch(value.type){
          case "M":
            this.messages.push(value);
          break;
          case "T":
            this.tp.getById(value.data).valueChanges().subscribe((data2) =>{
              this.tasks.push(data2);
              console.log(data2);
            });
          break;
        }
      });
    });

    //tasks
    this.up.getTasks(this.ap.currentFirestoreUser).valueChanges().subscribe((data) =>{
      this.usersTasks = [];
      data.forEach((value) =>{
        this.tp.getById(value.tid).valueChanges().subscribe((data2) =>{
          let output: taskOutput = {
            usersTasks: data2,
            taskTid: value.tid
          }
          this.usersTasks.push(output);
        });
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatModalPage');
  }

  sendMessage(){
    if(this.msgToSend != null && this.msgToSend != ""){
      this.cp.send(this.ap.currentUser.authid, this.cp.getChatId(this.ap.currentUser.authid, this.friendsChatInfo.authid), "M", this.msgToSend);
      this.msgToSend = "";
    }
  }

  sendTask(selectedTaskTid){
    if(selectedTaskTid != null){
      this.cp.send(this.ap.currentUser.authid, this.cp.getChatId(this.ap.currentUser.authid, this.friendsChatInfo.authid), "T", selectedTaskTid);
    }
  }

  closeChatModal(){
    this.vc.dismiss();
  }

}
