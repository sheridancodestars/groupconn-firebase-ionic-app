import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { User, UserProvider } from '../../providers/user/user';
import { FrequestProvider } from '../../providers/frequest/frequest';
import { AuthProvider } from '../../providers/auth/auth';
import { DocumentChangeAction } from 'angularfire2/firestore/interfaces';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';


@Component({
  selector: 'page-friends-add',
  templateUrl: 'friend_AddPage.html'
})
export class FriendsAddPage {

  loadedUsers;
  usersFiltered;

  loadedFriends;

  currUser: User;
  userName;

  constructor
  (
    private params: NavParams,
    public navCtrl: NavController, 
    private alertCtrl: AlertController, 
    private usr : UserProvider,
    private frq : FrequestProvider,
    public ap : AuthProvider
  ) 
  {
    this.currUser = this.ap.currentUser;
    this.userName = this.currUser.username;
  }

  ionViewWillEnter()
  {
    this.usr.users.ref.orderBy('username').get().then((data1) => {
      this.loadedUsers = [];
      this.usersFiltered = [];
      data1.docs.forEach((value1, index1, array1) => {
        this.loadedUsers.push(value1);
        this.usersFiltered.push(value1);
      });
    });

    this.usr.getFriends(this.ap.currentFirestoreUser).ref.get().then((data2) => {
      this.loadedFriends = [];
      data2.docs.forEach((value2, index2, array2) => {
        this.usr.getById(value2.data().uid).ref.get().then((data3) => {
          this.loadedFriends.push(data2);
        });
      });
    });
  }

  doRefresh(refresher) 
  {
    console.log('Begin async operation', refresher);

    this.usr.users.ref.orderBy('username').get().then((data1) => {
      this.loadedUsers = [];
      this.usersFiltered = [];
      data1.docs.forEach((value1, index1, array1) => {
        this.loadedUsers.push(value1);
        this.usersFiltered.push(value1);
      });
    });

    this.usr.getFriends(this.ap.currentFirestoreUser).ref.get().then((data2) => {
      this.loadedFriends = [];
      data2.docs.forEach((value2, index2, array2) => {
        this.usr.getById(value2.data().uid).ref.get().then((data3) => {
          this.loadedFriends.push(data2);
        });
      });
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  setUsers()
  {
    this.usersFiltered = this.loadedUsers;
  }

  searchForUser(ev)
  { 
    this.setUsers();
    let inputVal = ev.target.value;

    if(!inputVal){
      return;
    }

    this.usersFiltered = this.usersFiltered.filter((data) =>{
      if(data.data().username && inputVal){
        if(data.data().username.toLowerCase().indexOf(inputVal.toLowerCase()) > -1){
          return true;
        }
        return false;
      }
    });
  }

  sendRequest(userid, username){
        let reqAlert = this.alertCtrl.create({
          title: 'Want to send a request to: ' + username,
          buttons: [
            {
              text: 'Send',
              handler: () => 
              {
                this.frq.send(this.ap.currentFirestoreUser, this.usr.getById(userid));
              }
            },
            {
              text: 'cancel',
              handler: () => 
              {
                
              }
            }
          ]
        });
        reqAlert.present();
      }
}
