import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http'



// Providers
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { GooglePlus } from '@ionic-native/google-plus';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { UserProvider } from '../providers/user/user';
import { GroupProvider } from '../providers/group/group';
import { Task2Provider } from '../providers/task2/task2';
import { AuthProvider } from '../providers/auth/auth';
import { FrequestProvider } from '../providers/frequest/frequest';
// for uploading profile image
import { FilePath } from '@ionic-native/file-path';
import { ChatProvider } from '../providers/chat/chat';
// for adding tasks to Calendar
import { Calendar } from '@ionic-native/calendar';

// Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyBg-4tFdgGpgFXKZOGRMCFdk1c77yeyAyg",
  authDomain: "groupconn-test.firebaseapp.com",
  databaseURL: "https://groupconn-test.firebaseio.com",
  projectId: "groupconn-test",
  storageBucket: "groupconn-test.appspot.com",
  messagingSenderId: "177359444465"
};

import { MyApp } from './app.component';

// Pages
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { TasksPage } from '../pages/tasks/tasks';
import { AddTaskPage } from '../pages/add-task/add-task';
import { FriendsPage } from '../pages/friends/friends';
import { FriendInfoDisplay } from '../pages/displayFriends/friends_Display';
import { FriendsRequestPage } from '../pages/friendRequest/friend_ReqPage';
import { FriendsAddPage } from '../pages/addFriend/friend_AddPage';
import { GroupsPage } from '../pages/groups/groups';
import { GroupDisplayPage } from '../pages/group-display/group-display';
import { JoinGroupPage } from '../pages/join-group/join-group';
import { GroupTaskCreatePage } from '../pages/group-task-create/group-task-create';
import { GroupTaskViewUpdatePage } from '../pages/group-task-view-update/group-task-view-update';
import { ChatModalPage } from '../pages/chat-modal/chat-modal';
import { GroupChatModalPage } from '../pages/group-chat-modal/group-chat-modal';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { LocationPage } from '../pages/location/location';
import { NewUserPage } from '../pages/new-user/new-user';
import { ProfilePage} from '../pages/profile/profile';
import { TaskDisplayPage } from '../pages/task-display/task-display';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    TasksPage,
    AddTaskPage,
    FriendsPage,
    FriendInfoDisplay,
    FriendsRequestPage,
    FriendsAddPage,
    GroupsPage,
    GroupDisplayPage,
    JoinGroupPage,
    GroupTaskCreatePage,
    GroupTaskViewUpdatePage,
    ChatModalPage,
    GroupChatModalPage,
    RegisterPage,
    LoginPage,
    NewUserPage,
    ProfilePage,
    LocationPage,
    TaskDisplayPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    TasksPage,
    AddTaskPage,
    FriendsPage,
    FriendInfoDisplay,
    FriendsRequestPage,
    FriendsAddPage,
    GroupsPage,
    GroupDisplayPage,
    JoinGroupPage,
    GroupTaskCreatePage,
    GroupTaskViewUpdatePage,
    ChatModalPage,
    GroupChatModalPage,
    RegisterPage,
    LoginPage,
    NewUserPage,
    ProfilePage,
    LocationPage,
    TaskDisplayPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    GoogleMaps,
    UserProvider,
    GroupProvider,
    Task2Provider,
    AuthProvider,
    FrequestProvider,
    File,
    Camera,
    Calendar,
    FilePath,
    GooglePlus,
    ChatProvider
  ]
})
export class AppModule {}
