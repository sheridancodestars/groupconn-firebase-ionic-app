import { Component } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

import { AuthProvider } from '../providers/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage : any = null;

  constructor(
    platform : Platform,
    statusBar : StatusBar,
    splashScreen : SplashScreen,
    ap : AuthProvider,
    toastCtrl: ToastController
  ) {
    platform.ready().then(() => {
      if(ap.currentUser == null)
      {
        this.rootPage = LoginPage;
      }

      ap.isAuthorized$.subscribe((data) => {
        if(data)
        {
          // Authenticated, set root page to home page (tabs).
          if(this.rootPage != TabsPage)
          {
            toastCtrl.create({
              message : "Logged in!",
              duration : 3000,
              position : "top"
            }).present();

            this.rootPage = TabsPage;
          }
        }
        else
        {
          // Logged out, set root page to login page.
          if(this.rootPage == TabsPage)
          {
            toastCtrl.create({
              message : "You have been logged out.",
              duration : 3000,
              position : "top"
            }).present();

            this.rootPage = LoginPage;
          }
        }
      });

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

