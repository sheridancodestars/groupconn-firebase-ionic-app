# GroupConn
GroupConn is a social platform mainly focused on group productivity. It allows students to quickly share timelines and schedules with each other and provides useful tools that help facilitate group communication.

## Technologies
GroupConn is built on the [Ionic Framework](http://ionicframework.com) and the [Firebase SDK](https://firebase.google.com), to maintain scalability and cross-compatibility.

## Development Prerequisites
* `node >= 8`
* `npm >= 5`
* npm `ionic@latest` and `cordova@latest`

## Compiling GroupConn
* `git clone`
* `npm i`
* `ionic cordova resources`
* `ionic cordova prepare`
* For testing and development: `ionic serve`
* For Android: `ionic cordova build android`
* For iOS (Mac only): `ionic cordova build ios`

## CodeStars
* John Olano
* Harpriya Kaur
* Gia Thanh Huynh
* Jashandeep Singh
